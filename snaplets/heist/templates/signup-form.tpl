<form class="form registration" method="post" action="${postAction}">

  <p class="name">
    <label for="name">Login</label>
    <input type="text" name="login" size="20" />
  </p>

  <p class="password">
    <label for="password">Password</label>
    <input type="password" name="password" size="20" /> <br/>
    <input type="password" name="verification" size="20" />
  </p>

  <p class="submit">
    <input type="submit" id="login_button" value="${submitText}" />
  </p>

</form>
