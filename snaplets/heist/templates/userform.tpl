<form class="form loginForm" method="post" action="${postAction}">
  <p class="name">
    <input type="text" name="login" size="20" />
    <label for="name">Login</label>
  </p>

  <p class="password">
    <input type="password" name="password" size="20" />
    <label for="password">Password</label>
  </p>

  <p class="submit">
    <input type="submit" id="login_button" value="${submitText}" />
    <a href="/new_user/" id="sign_up">Sign Up</a>
  </p>
</form>