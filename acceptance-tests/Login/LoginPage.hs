{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}
module Login.LoginPage where

import           Data.Text
import           Test.WebDriver
------------------------------------------------------------------------------

loginPage :: String
loginPage = "http://localhost:8000/login"

------------------------------------------------------------------------------
enterUsername :: Text -> WD ()
enterUsername username = enterInput username =<< usernameInput 

------------------------------------------------------------------------------
enterPassword :: Text -> WD ()
enterPassword password = enterInput password =<< passwordInput

------------------------------------------------------------------------------
enterInput :: Text -> Element -> WD ()
enterInput !text !element = sendKeys text element

------------------------------------------------------------------------------
usernameInput :: WD Element
usernameInput = findElem $ ByName "login"

------------------------------------------------------------------------------
passwordInput :: WD Element
passwordInput = findElem $ ByName "password"

------------------------------------------------------------------------------
authErrorMessage :: WD Element
authErrorMessage = findElem $ ByClass "error"

------------------------------------------------------------------------------
loginButton :: WD Element
loginButton = findElem $ ById "login_button"

------------------------------------------------------------------------------
clickLoginButton :: WD ()
clickLoginButton = click =<< loginButton
