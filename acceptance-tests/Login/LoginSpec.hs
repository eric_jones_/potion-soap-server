{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings #-}
module Login.LoginSpec (loginTest) where

import Control.Monad.IO.Class
import Test.HUnit
import Test.WebDriver
import Text.Regex.Posix
------------------------------------------------------------------------------
import Login.LoginPage

------------------------------------------------------------------------------
loginTest :: WD ()
loginTest = do redirectToLaboratoryPageTest
               badAthenticationErrorTest

------------------------------------------------------------------------------
redirectToLaboratoryPageTest :: WD ()
redirectToLaboratoryPageTest = do
  givenIAmOnTheLoginPage
  whenISubmitValidAuthenticationCredentials
  thenIShouldBeRedirectedToTheLaboratoryPage 

------------------------------------------------------------------------------
badAthenticationErrorTest :: WD ()
badAthenticationErrorTest = do
  givenIAmOnTheLoginPage
  whenISubmitBadAuthenticationData
  thenIShouldSeeAnErrorMessage

------------------------------------------------------------------------------
givenIAmOnTheLoginPage :: WD ()
givenIAmOnTheLoginPage = openPage loginPage

------------------------------------------------------------------------------
whenISubmitValidAuthenticationCredentials :: WD ()
whenISubmitValidAuthenticationCredentials = do
  enterUsername "user"
  enterPassword "password"
  clickLoginButton

------------------------------------------------------------------------------
whenISubmitBadAuthenticationData :: WD ()
whenISubmitBadAuthenticationData = do
  enterUsername "Bad username"
  enterPassword "Bad password"
  clickLoginButton

------------------------------------------------------------------------------
thenIShouldSeeAnErrorMessage :: WD ()
thenIShouldSeeAnErrorMessage = do
  errorElement <- authErrorMessage
  displayed    <- isDisplayed errorElement
  liftIO $ displayed @? "Authentication error message was not displayed."

------------------------------------------------------------------------------
thenIShouldBeRedirectedToTheLaboratoryPage :: WD () 
thenIShouldBeRedirectedToTheLaboratoryPage = do
  url <- getCurrentURL
  let result = url =~ ("http://localhost:8000/#laboratory.*" :: String) :: Bool
  liftIO $ result @? "The url did not indicate redirection to laboratory page."




