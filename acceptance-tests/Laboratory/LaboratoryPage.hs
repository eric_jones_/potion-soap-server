{-# LANGUAGE OverloadedStrings #-}
module Laboratory.LaboratoryPage where

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict
import Test.WebDriver

------------------------------------------------------------------------------
data LaboratoryPageState = LoginPageState {ingredientsListCount :: Integer}

------------------------------------------------------------------------------
type LaboratoryPageAction a = StateT LaboratoryPageState WD a

------------------------------------------------------------------------------
runLaboratoryPageAction :: LaboratoryPageAction () -> WD ()
runLaboratoryPageAction =  (`evalStateT` initialPageState)

------------------------------------------------------------------------------
initialPageState :: LaboratoryPageState
initialPageState = LoginPageState 0

------------------------------------------------------------------------------
anyReagentIcon :: WD Element
anyReagentIcon = findElem $ ByClass "reagent-icon"

------------------------------------------------------------------------------
numberOfIngredientsInList :: LaboratoryPageAction Integer
numberOfIngredientsInList = do
  ingredientsAreaElement <- lift ingredientsList
  ingredientElements     <- lift $ findElemsFrom ingredientsAreaElement
                                                 (ByTag "li")
  return . fromIntegral $ length ingredientElements 

------------------------------------------------------------------------------
anyAddOrRemoveReagentLink :: WD Element  
anyAddOrRemoveReagentLink = do reagentElem <- anyReagentIcon
                               findElemFrom reagentElem $ 
                                 ByClass "add-or-remove-ingredient-link"

------------------------------------------------------------------------------
clickAnyAddReagentLink :: WD ()
clickAnyAddReagentLink = click =<< anyAddOrRemoveReagentLink

------------------------------------------------------------------------------
ingredientsList :: WD Element
ingredientsList = findElem $ ById "soap-reagents"
