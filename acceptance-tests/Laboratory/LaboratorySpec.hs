{-# LANGUAGE OverloadedStrings #-}
module Laboratory.LaboratorySpec (laboratoryTest) where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict 
import Test.HUnit
import Test.WebDriver
import Test.WebDriver.Commands.Wait
------------------------------------------------------------------------------
import Login.LoginPage
import Laboratory.LaboratoryPage

------------------------------------------------------------------------------
laboratoryTest :: WD ()
laboratoryTest = runLaboratoryPageAction $ do
  displayReagentsTest
  addReagentToIngredients
  removeReagentFromIngredients

------------------------------------------------------------------------------
displayReagentsTest :: LaboratoryPageAction ()
displayReagentsTest = do
  givenIAmOnTheLaboratoryPage
  thenIShouldSeeReagents 

------------------------------------------------------------------------------
addReagentToIngredients :: LaboratoryPageAction ()
addReagentToIngredients = do 
  givenIAmOnTheLaboratoryPage
  whenIClickOnAReagentsAddLink
  thenTheReagentShouldBeAddedToTheIngredientsList

------------------------------------------------------------------------------
removeReagentFromIngredients :: LaboratoryPageAction ()
removeReagentFromIngredients = do 
  givenIAmOnTheLaboratoryPage
  andIHaveAddedAParticularReagentToTheIgredientsList
  whenIClickOnAReagentsAddLink
  thenThatReagentShouldBeRemovedFromTheList

------------------------------------------------------------------------------
thenThatReagentShouldBeRemovedFromTheList :: LaboratoryPageAction ()
thenThatReagentShouldBeRemovedFromTheList = do
  oldIngredientCount <- liftM ingredientsListCount get
  waitForIngredientToBeRemoved
  newIngredientCount <- numberOfIngredientsInList
  lift $ assertLessThan newIngredientCount
                        oldIngredientCount
                        errorMessage
  where 
    errorMessage = "A reagent was not removed to the ingredients list."                                           

------------------------------------------------------------------------------
andIHaveAddedAParticularReagentToTheIgredientsList :: LaboratoryPageAction ()
andIHaveAddedAParticularReagentToTheIgredientsList = whenIClickOnAReagentsAddLink

------------------------------------------------------------------------------
givenIAmOnTheLaboratoryPage :: LaboratoryPageAction ()
givenIAmOnTheLaboratoryPage = lift $ do
  login
  waitForPageLoad

------------------------------------------------------------------------------
whenIClickOnAReagentsAddLink :: LaboratoryPageAction ()
whenIClickOnAReagentsAddLink = do
  numberOfIngredients <- numberOfIngredientsInList 
  put $ LoginPageState numberOfIngredients
  lift clickAnyAddReagentLink

------------------------------------------------------------------------------
thenTheReagentShouldBeAddedToTheIngredientsList :: LaboratoryPageAction ()
thenTheReagentShouldBeAddedToTheIngredientsList = do
  oldIngredientCount <- liftM ingredientsListCount get
  waitForNewIngredientToBeAdded
  newIngredientCount <- numberOfIngredientsInList
  lift $ assertGreaterThan newIngredientCount
                           oldIngredientCount
                           errorMessage
  where 
    errorMessage = "A reagent was not added to the ingredients list."   

------------------------------------------------------------------------------
thenIShouldSeeReagents :: LaboratoryPageAction ()
thenIShouldSeeReagents = do
  numReagentIcons <- liftM length $ findElems (ByClass "reagent-icon")
  lift $ assertGreaterThan numReagentIcons
                           noReagentIcons
                           errorMessage
 where errorMessage   = "No reagent-icon elements were found on the page."
       noReagentIcons = 0
       
------------------------------------------------------------------------------
login :: WD ()
login = do
  openPage loginPage
  enterUsername "user"
  enterPassword "password"
  clickLoginButton

------------------------------------------------------------------------------
waitForNewIngredientToBeAdded :: LaboratoryPageAction ()
waitForNewIngredientToBeAdded = waitForChangeInIngredientListCount (<)

------------------------------------------------------------------------------
waitForIngredientToBeRemoved :: LaboratoryPageAction ()
waitForIngredientToBeRemoved = waitForChangeInIngredientListCount (>)

------------------------------------------------------------------------------
waitForChangeInIngredientListCount :: (Integer -> Integer -> Bool) 
                                   -> LaboratoryPageAction ()
waitForChangeInIngredientListCount comparitor = waitUntil 0.5 $ do
  oldIngredientCount <- liftM ingredientsListCount get
  waitCondition      <- do newIngredientCount <- numberOfIngredientsInList
                           return $ comparitor oldIngredientCount  
                                               newIngredientCount
  expect waitCondition   

------------------------------------------------------------------------------
waitForPageLoad :: WD ()
waitForPageLoad = waitUntil 1.0 $ do
  pageLoaded <- executeJS [] "return document.readyState == \"complete\""
  expect pageLoaded

------------------------------------------------------------------------------
assertGreaterThan :: (Ord a, Show a) => a -> a -> String -> WD ()
assertGreaterThan x y msg = liftIO $ (x > y) @? msg

------------------------------------------------------------------------------
assertLessThan :: (Ord a, Show a) => a -> a -> String -> WD ()
assertLessThan x y msg = liftIO $ (x < y) @? msg
