{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Test.WebDriver
import           Test.Tasty
import           Test.Tasty.HUnit
------------------------------------------------------------------------------
import           Login.LoginSpec
import           Laboratory.LaboratorySpec

------------------------------------------------------------------------------
main :: IO ()
main = defaultMain $ testGroup "Potion Soap acceptance tests" tests

------------------------------------------------------------------------------
tests :: [TestTree]
tests = map (\ (wd,msg) -> testCase msg $ runSession defaultSession defaultCaps wd) [ (loginTest, "Login page test")
                                                                                    , (laboratoryTest, "Laboratory page test")
                                                                                    ]
