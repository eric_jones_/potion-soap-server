{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MonadComprehensions #-}

module Authentication.Site where


import           Control.Applicative    ((<|>))

import           Data.ByteString        (ByteString)
import           Data.Maybe
import qualified Data.Text              as T
import qualified Heist.Interpreted      as I
import           Heist 
import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.AcidState
import           Snap.Snaplet.Auth
import           Snap.Snaplet.Heist
------------------------------------------------------------------------------
import           Application


------------------------------------------------------------------------------
laboratoryURL :: ByteString
laboratoryURL = "/#laboratory"


------------------------------------------------------------------------------
handleLogin :: Maybe T.Text -> Handler App (AuthManager App) ()
handleLogin authError = heistLocal (I.bindSplices errs) $ render "login"
  where
    errs = maybe noSplices splice authError
    splice err = "loginError" ## I.textSplice err

------------------------------------------------------------------------------
handleLoginSubmit :: Handler App (AuthManager App) ()
handleLoginSubmit =
    loginUser "login" "password" Nothing
              (\_ -> handleLogin $ Just err)
              (redirect laboratoryURL)
  where
    err = "Unknown user or password"


------------------------------------------------------------------------------
handleLogout :: Handler App (AuthManager App) ()
handleLogout = logout >> redirect "/login"


------------------------------------------------------------------------------
handleNewUser :: Handler App (AuthManager App) ()
handleNewUser = method GET handleForm <|> method POST handleFormSubmit
  where
    handleForm = render "new_user"

    handleFormSubmit = do
    eitherAuthFailureOrUser <- registerUser "login" "password"
    case eitherAuthFailureOrUser of
      Left _     -> redirect "/login"
      Right user -> do
         maybeRole <- getPostParam "role"
         case maybeRole of
           (Just role) -> do
             eitherErrorUser <- saveUser $ user { userRoles = [Role role] }
             case eitherErrorUser of
               (Right user) -> do
                 let uid = fromJust . userId $ user
                 update (CreatePotionMaker uid)
                 redirect "/login"
               (Left _) -> error ""
           Nothing -> error "error"


------------------------------------------------------------------------------
routes :: [(ByteString, Handler App App ())]
routes = [ ("/login"        , with auth (handleLogin Nothing))
         , ("/login_submit" , with auth handleLoginSubmit)
         , ("/logout"       , with auth handleLogout)
         , ("/new_user"     , with auth handleNewUser)
         ]
