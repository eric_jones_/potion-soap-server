{-# LANGUAGE OverloadedStrings #-}

module CSS.ReagentIcon where

import Clay

------------------------------------------------------------------------------
reagentIconCss :: Css
reagentIconCss = ".reagent-icon-image" ?
  do display   inlineBlock
     width     (px 200)
     padding   (px 0) (px 26) (px 0) (px 26)
     textAlign (alignSide sideLeft)


------------------------------------------------------------------------------
mixtureCss :: Css
mixtureCss = "#soap-creation-area" ?
  do marginRight (px 294)
     height      (pct 100)
     width       (pct 50)
     position    absolute
     top         0


------------------------------------------------------------------------------
buttonCss :: Css
buttonCss = ".button" ?
  do width      (auto :: (Size a))
     padding    (em 0.5) (em 0.55) (em 0.5) (em 0.55)
     background ("#617798" :: Color)
     border     solid (px 0) (rgba 0 0 0 255)
     fontSize   (px 14)
     color      "#FFFFFF"
     textDecoration  none

------------------------------------------------------------------------------
orangeGrandientCss :: Css
orangeGrandientCss = ".orange" ? return ()

------------------------------------------------------------------------------
textAreaCss :: Css
textAreaCss ="input, textArea" ?
  do padding    (px 9) (px 9) (px 9) (px 9)
     border     solid (px 1) "#E5E5E5"
     outline    solid (px 0) (rgba 0 0 0 255)
     width      (px 200)
     fontFamily ["Verdana", "Tahoma"] [sansSerif]
     fontSize   (px 13)
     background ("#FFFFFF" :: Color)
     boxShadow  (px 0) (px 0) (px 8) (rgba 0 0 0 10)
     background $
       linearGradient (straight sideTop)
	 [ ("#ffffff", pct 0.1)
	 , ("#eeeeee", pct 14.9)
	 , ("#ffffff", pct 85.0)
	 ]


------------------------------------------------------------------------------
formLabelCss :: Css
formLabelCss = ".form label" ?
  do marginLeft (px 10)
     color "#999999"


------------------------------------------------------------------------------
submitButton :: Css
submitButton = ".submit input" ?
  do width      (auto :: (Size a))
     padding    (px 9) (px 15) (px 9) (px 15)
     background ("#617798" :: Color)
     border     solid (px 0) (rgba 0 0 0 255)
     fontSize   (px 14)
     color      "#FFFFFF"


------------------------------------------------------------------------------
inputHover :: Css
inputHover = "input:hover, input:focus, textArea:focus, textArea:Hover" ?
  do
    borderColor "#C9C9C9"
    boxShadow  (px 0) (px 0) (px 8) (rgba 0 0 0 15)


------------------------------------------------------------------------------
loginSectionCss :: Css
loginSectionCss = ".loginSection" ?
 do background ("#EEEEEE" :: Color)
    width      (em 20)
    border     solid (px 1) "#DDDDDD"
    margin     (em 0.75) (em 0.75) (em 0.75) (em 0.75) 


------------------------------------------------------------------------------
loginFormCss :: Css
loginFormCss = ".loginForm" ?
  do
    size   (auto :: Size a)
    margin (em 1.5) (em 0.5) (em 1.5) (em 0.5)


------------------------------------------------------------------------------
signUp :: Css
signUp = "#sign_up" ?
  do width           (auto :: (Size a))
     padding         (px 9) (px 15) (px 9) (px 15)
     background      ("#617798" :: Color)
     border          solid (px 0) (rgba 0 0 0 255)
     fontSize        (px 14)
     color           "#FFFFFF"
     textDecoration  none
