define([ 'backbone'
       ],

function(Backbone){
  var ReagentModel = Backbone.Model.extend({
    urlRoot : "/api/reagents",    
     
    name : function() {
      return this.get('name');
    }
  }, {
    ADD_OR_REMOVE_INGREDIENT_LINK_CLICKED: "add-or-remove-link-clicked"
  });

  return ReagentModel;
});
