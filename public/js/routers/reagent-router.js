define([ 'backbone'
       , 'backbone-extensions/router-utilities'
       ],

function( Backbone 
	, RouterUtils
	){
    
  var ReagentRouter = Backbone.Router.extend({

    routes: { 'laboratory'   : 'laboratory'
            , 'reagentInput' : 'inputReagent'
	    , '*path'        : 'default'
            },

    initialize : function(laboratoryView, reagentInputView){
      this.laboratoryView        = laboratoryView;
      this.reagentInputView      = reagentInputView;
      this.routeChangeObservable = RouterUtils.createObservable('route:laboratory', this);
    },

    run : function(){
      this.on('route:default',      function(){this.navigate('laboratory', true);});
      this.on('route:laboratory',   function(){RouterUtils.openView(this.laboratoryView);});
      this.on('route:inputReagent', function(){RouterUtils.replaceContentWith(this.reagentInputView);});
      }
  });

  return ReagentRouter;
});
