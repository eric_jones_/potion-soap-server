define([ 'backbone'
       , 'backbone-extensions/router-utilities'
       , 'views/login-view'
       ],

function( Backbone 
	, RouterUtils
        , LoginView
	){
    
  var LoginRegistrationRouter = Backbone.Router.extend({

    routes: { 'loginC'       : 'loginC'
            , 'registration' : 'registration'
            },

    initialize : function(loginView){
      this.loginView             = loginView;
      this.routeChangeObservable = RouterUtils.createObservable('route:loginC', this);
    },

    run : function(){
      this.on('route:loginC', function(){RouterUtils.openView(this.loginView);});
      }
  });

  return LoginRegistrationRouter;
});
