define([ 'underscore',       
         'models/reagent-model',
         'views/add-or-remove-link-view',
         'views/reagent-icon-view'
       ],

function ( _, 
           ReagentModel,
           AddOrRemoveLinkView,
           ReagentIconView
         ){
  var ReagentIconViewFactory = function() {};

  _.extend(ReagentIconViewFactory.prototype, {
    
    createReagentIconView : function(reagentModel) {
      var addRemoveLinkView = new AddOrRemoveLinkView(reagent);
      var reagentIconView   = new ReagentIconView({model : reagent, addLinkView : addRemoveLinkView });      
      this.listenTo(addRemoveLinkView, ReagentModel.ADD_OR_REMOVE_INGREDIENT_LINK_CLICKED, this.iconClicked);
      return reagentIconView;
    }

  });

  return ReagentIconViewFactory;
});

