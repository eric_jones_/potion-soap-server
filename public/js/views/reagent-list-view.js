define([ 'backbone',
         'collections/reagents',
         'views/reagent-icon-view',
         'models/reagent-model',
         'views/add-or-remove-link-view',
         'text!/../templates/reagents_table_template.html'
       ],

function( Backbone,
          Reagents,
          ReagentIconView,
          ReagentModel,
          AddOrRemoveLinkView,
          ReagentsTemplate
    ){

  var ReagentListView = Backbone.View.extend({

    initialize: function(reagents, reagentIconFactory){

      this.collection = reagents;
      this.iconFactory = reagentIconFactory;

      this.listenTo(this.collection, "change", this.render);
      this.listenTo(this.collection, "add", this.render);

      var fetchOnInterval = function() { this.collection.fetch(); };
      fetchOnInterval = _.bind(fetchOnInterval, this);
      self.setInterval(fetchOnInterval, 10000000);
    },

    iconClicked : function(args){
      this.trigger(ReagentModel.ADD_OR_REMOVE_INGREDIENT_LINK_CLICKED, args);
    },

    render: function() {
      var template = _.template(ReagentsTemplate,{});
      this.$el.html(template);
      this.setElement(template);

      var reagentModels = this.collection.models;

      var iconViews = _.map(reagentModels, this.iconFactory.createReagentIconView, this);
      _.each(iconViews, function(view){this.$el.append(view.render().el);}, this);
      return this;
    }

  });

  return ReagentListView;
});
