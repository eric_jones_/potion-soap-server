define([ 'backbone',
         'models/reagent-model',
         'text!/../templates/reagent_icon_template.html',
         'views/add-or-remove-link-view',
         'backbone-extensions/view-utilities',
         'bootstrap'
       ],

function(Backbone, ReagentModel, ReagentIconTemplate, AddOrRemoveLinkView, ViewUtils){
  var ReagentIconView = Backbone.View.extend({
        
    initialize: function(){
      this.model       = this.options.model;
      this.addLinkView = this.options.addLinkView;
    },

    templateBindings: function (){
      return { reagentName      : this.model.get("name"),
               imageUrl         : this.model.get("imageUrl"),
               imageNotFoundUrl : "images/reagent-icons/unavailable/imageNotFoundUrl",
               isImageUndefined : this.isImageUndefined()
             };
    },

    isImageUndefined: function(){
        return typeof(this.model.get("imageUrl")) != "undefined";
    },

    disableImageDragEffect : function(){
       _.first(this.$('.reagent-icon-image')).ondragstart = function() { return false; };
    },

    render: function(){
      this.renderTemplate(ReagentIconTemplate, this.templateBindings());
      var addOrRemoveLinkElement = this.addLinkView.render().el;
      this.$('#add-or-remove-link-placeholder').replaceWith(addOrRemoveLinkElement);
      this.disableImageDragEffect();
      return this;
    }
  });

  return ReagentIconView;
});
