define([ 'backbone'		
       , 'text!/../templates/login_template.html'
       , 'backbone-extensions/view-utilities'
       , 'backbone-extensions/view-extensions'
       ],

function( Backbone
	, LoginTemplate
	, ViewUtils
	){

  var LoginView = Backbone.View.extend({

    initialize : function (){
      this.changeRouteObserver = ViewUtils.createRouteChangeObserver(this);
    },

    render: function() {
      this.renderTemplate(LoginTemplate);
      return this;
    }
  });

  return LoginView;

});
