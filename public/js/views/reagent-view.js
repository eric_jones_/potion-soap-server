define([ 'backbone'
       , 'models/reagent-model'
       , 'text!/../templates/reagent_template.html'
       , 'backbone-extensions/view-extensions'
       ],

function(Backbone, ReagentModel, ReagentTemplate){
    var ReagentView = Backbone.View.extend({

      initialize : function (reagent){
	this.model = reagent;
      },

      render: function() {
	var name = this.model.name;
	this.renderTemplate(ReagentTemplate, { name : name } );
	return this;
      }
  });

  return ReagentView;
});
