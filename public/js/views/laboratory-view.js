define([ 'backbone',
         'rx',
         'views/reagent-list-view',
         'views/soap-creation-view',
         'text!/../templates/laboratory.html',
         'models/reagent-model',
         'backbone-extensions/view-utilities',   
         'backbone-extensions/view-extensions'
       ],

function( Backbone,
          Rx,
          ReagentListView,
          SoapCreateionView,
          LaboratoryTemplate,
          ReagentModel,
          ViewUtils
        ){

  var LaboratoryView = Backbone.View.extend({

    initialize : function(reagentListView, soapCreationView) {
      this.reagentList         = reagentListView;
      this.soapCreationView    = soapCreationView;
      this.changeRouteObserver = ViewUtils.createRouteChangeObserver(this);
      this.open                = _.bind(this.open, this);
    },

    open : function(){
      this.soapCreationView.open();
      this.listenTo(this.reagentList, ReagentModel.ADD_OR_REMOVE_INGREDIENT_LINK_CLICKED, this.addToMixture);
    },

    addToMixture : function(args) {
      this.soapCreationView.addReagent(args);
    },
    
    render: function() {
      this.renderTemplate(LaboratoryTemplate, {});

      var reagentListElement  = this.reagentList.render().el;
      var soapCreationElement = this.soapCreationView.render().el;

      this.$('.reagents').replaceWith(reagentListElement);
      this.$('.soap').replaceWith(soapCreationElement);

      return this;
    }   

  });

  return LaboratoryView;

});
