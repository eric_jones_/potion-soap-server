define([ 'backbone',
         'models/reagent-model',
         'text!/../templates/add_or_remove_link.html',
         'backbone-extensions/view-utilities'
       ],

function(Backbone,
         ReagentModel, 
         AddOrRemoveLinkTemplate, 
         ViewUtils){
  var AddOrRemoveLinkView = Backbone.View.extend({

    events : { 'click' : 'addOrRemoveLinkClicked' },

    initialize: function(reagent){
      this.model = reagent;
    },

    addOrRemoveLinkClicked : function(linkClickEvent) {
      linkClickEvent.preventDefault();
      this.trigger(ReagentModel.ADD_OR_REMOVE_INGREDIENT_LINK_CLICKED, this.model);
    },

    render: function(){
      this.renderTemplate(AddOrRemoveLinkTemplate);
      return this;
    }
  });

  return AddOrRemoveLinkView;

});
