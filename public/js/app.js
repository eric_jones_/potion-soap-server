define([ 'backbone',
         'rx',
         'routers/reagent-router',
         'routers/mixture-router',
         'views/reagent-list-view',
         'views/soap-creation-view',
         'views/laboratory-view',
         'views/reagent-input-view',
         'views/potionmakers-mixture-view',
         'models/reagent-model',
         'collections/reagents',
         'models/mixture',
         'views/mixture-view',
         'models/reagent-icon-factory'
       ],

function( Backbone,
          Rx,
          ReagentRouter,
          MixtureRouter,
          ReagentListView,
          SoapCreationView,
          LaboratoryView,
          ReagentInputView,
          PotionMakersMixturesView,
          Reagent,
          Reagents,
          Mixture,
          MixtureView,
          ReagentIconViewFactory
        ){

  var bootstrapedReagents      = new Reagents(self.reagents);
  var bootstrapedMixtures      = _.map(self.mixtures, function(mixtureData){ return new Mixture(mixtureData); });
  var mixtureViews             = _.map(bootstrapedMixtures, function(mixture){ return new MixtureView(mixture); });

  var reagentModel             = new Reagent();
  var reagentCollection        = new Reagents();
  var reagentInputView         = new ReagentInputView(reagentModel, reagentCollection);
 
  var reagentIconViewFactory   = new ReagentIconViewFactory();

  var mixture                  = new Mixture();
  var soapCreationView         = new SoapCreationView(mixture);
  var reagentListView          = new ReagentListView(bootstrapedReagents, reagentIconViewFactory);
  var laboratoryView           = new LaboratoryView(reagentListView, soapCreationView);

  var potionMakersMixturesView = new PotionMakersMixturesView(mixtureViews);

  var reagentRouter            = new ReagentRouter(laboratoryView, reagentInputView);
  var mixtureRouter            = new MixtureRouter(potionMakersMixturesView);

  mixtureRouter.routeChangeObservable
            .subscribe(laboratoryView.changeRouteObserver);

  reagentRouter.routeChangeObservable
            .subscribe(potionMakersMixturesView.changeRouteObserver);

  var initialize = function(){
    runRouters();
    Backbone.history.start();
  };

  var runRouters = function(){
    reagentRouter.run();
    mixtureRouter.run();
  };

  return { initialize: initialize };
});
